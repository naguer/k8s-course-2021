sudo snap install microk8s --classic --channel=1.21
sudo usermod -a -G microk8s $USER
sudo chown -f -R $USER ~/.kube
microk8s status --wait-ready
microk8s kubectl get nodes
microk8s kubectl get services
alias kubectl='microk8s kubectl'
microk8s kubectl create deployment nginx --image=nginx
microk8s kubectl get pods
microk8s stop
microk8s start
