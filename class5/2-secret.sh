echo -n 'admin' > username
echo -n '1f2d1e2e67df' > password
kubectl create secret generic db-user-pass --from-file=username --from-file=password
# kubectl create secret generic db-user-pass --from-file=username=./username.txt --from-file=password=./password.txt
# kubectl create secret generic db-user-pass --from-literal=username=devuser --from-literal=password='S!B\*d$zDsb='
kubectl get secrets
kubectl describe secrets/db-user-pass
kubectl get secret db-user-pass -o jsonpath='{.data}'
echo 'MWYyZDFlMmU2N2Rm' | base64 --decode
# echo -n 'my-app' | base64

kubectl create secret generic backend-user --from-literal=backend-username='backend-admin'
kubectl create -f 2-pod-with-secret.yml
kubectl get secrets/backend-user -o yaml
kubectl exec -it env-single-secret -- /bin/sh -c 'echo $SECRET_USERNAME'
