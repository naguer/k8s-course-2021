# https://matthewpalmer.net/kubernetes-app-developer/articles/configmap-diagram.gif
kubectl apply -f 1-configmap.yml
kubectl describe cm example-configmap
kubectl create -f 1-pod-with-configmap.yml
kubectl exec -it pod-env-var -- env

