kubectl create namespace class3
kubectl explain namespace --recursive | less
kubectl config set-context --current --namespace=class3
kubectl config view --minify | grep namespace:
kubectl api-resources --namespaced=true
kubectl api-resources --namespaced=false
