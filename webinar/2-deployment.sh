kubectl create deployment --image=nginx nginx -n webinar-k8s --dry-run=client -o yaml > 2-deploy.yaml
kubectl create -f 2-deploy-modificado.yaml
kubectl get all -n webinar-k8s
kubectl port-forward nginx-XXX 8080:5005 -n webinar-k8s
kubectl delete pod nginx-* -n webinar-k8s
kubectl scale deploy nginx --replicas=3 -n webinar-k8s
kubectl get deploy nginx -n webinar-k8s
