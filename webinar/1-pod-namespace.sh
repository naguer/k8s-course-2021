kubectl create namespace webinar-k8s
kubectl run nginx --image=nginx -n webinar-k8s --dry-run=client -o yaml > pod-nginx-edit.yaml
kubectl create -f pod-nginx-edit.yaml
kubectl exec -ti nginx -n webinar-k8s -- /bin/bash
echo "WEBINAR K8S" > /usr/share/nginx/html/index.html
kubectl port-forward nginx -n webinar-k8s 8080:80
kubectl explain pod --recursive | less
kubectl delete pod nginx -n webinar-k8s
# edit pod-nginx-edit.yaml
command: [echo "WEBINAR K8S" > /usr/share/nginx/html/index.html]
